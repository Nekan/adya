<div align="center">![ShyrkaSystem](https://www.shyrkasystem.com/dl/logo-400.png)</div>

# Adya

<div align="center">![Adya](https://www.shyrkasystem.com/dl/adya/adya.png)</div>

Adya est un utilitaire d'assistance et de gestion des utilisateurs AD. Il facilite la création, la modification et la suppression des comptes utilisateurs.

## Mode d'emploi

Le mode d'emploi est consultable à l'adresse : [ShyrkaSystem](https://www.shyrkasystem.com/doku.php?id=adya)

## Dossiers

###  bin

Contient l'application pour les systèmes Windows 64 bits.

### images

Contient les images utilisées dans le programmes.

### export

Contient le script powershell non compilé. Utilisable directement depuis une console powershell.

### src

Contient les sources du programme, écrit avec SAPIEN PowerShell Studio.
