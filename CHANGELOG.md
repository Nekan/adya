# Adya - Suivi de version

## 3.3.0.0
- Correction de l'affichage du nom et du prénom dans l'onglet de modification
- Augmentation de la taille des mots de passe proposés (passage de 9 à 14 caractères)

## 3.2.0.1
- Correction des lien vers le site web et le contact

## 3.2.0.0
- Ajout d'une option pour obliger l'utilisateur à changer son mot de passe

## 3.1.0.2
- Correction orthographique

## 3.1.0.1
- Correction d'un bug sur l'indexation de la liste des groupes dans l'onglet "Modification"

## 3.1.0.0
- Ajout d'un écran de chargement et de vérification des prérequis au lancement de l'application
- Diverses corrections orthographiques

## 3.0.1.0
- Ajout d'une option pour toujours ajouter le groupe "Admins du domaine" à la liste des groupes

## 3.0.0.0
- Ajout d'une page de configuration pour paramétrer les valeurs par défaut :
-- Chemin de la racine des dossiers utilisateurs
-- Chemin de sauvegarde des dossier supprimés
-- Chemin de sauvegarde des informations AD des utilisateurs supprimés
-- Listes de OUs pour la création de la liste des groupes
- Mise en place d'un système de création et de gestion des profils par défaut
- Utilisation du registre pour la sauvegarde des préférences
- Ajout d'un système d'import/export de la configuration

## 2.0.0.0
- Modification du programme  et de l'interface pour une prise en charge dynamique des groupes de sécurité

## 1.0.1.0
- Correction d'un bug sur l'affichage des chemins par défaut dans la page de suppression

## 1.0.0.0
- Version de production

## 0.3.0.0
- Ajout de la fonctionnalité de suppression des utilisateurs

## 0.2.0.0
- Ajout de la fonctionnalité de modification des utilisateurs

## 0.1.0.0
- Première version
- Ajout de la fonctionnalité de création d'utilisateur